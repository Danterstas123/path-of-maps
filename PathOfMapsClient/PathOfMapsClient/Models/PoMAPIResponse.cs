﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PathOfMapsClient.Models {
    public class PoMAPIResponse {
        public bool RequestSucceeded() {
            return (command != null &&
                    command.command != null &&
                    map != null);
        }

        public Command command { get; set; }
        public Map map { get; set; }
        public object drop { get; set; }

        public class Prompt {
            public string character_name { get; set; }
        }

        public class Command {
            public Prompt prompt { get; set; }
            public Object command { get; set; }
        }

        public class Urls {
            public string hideout_url { get; set; }
        }

        public class User {
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public int id { get; set; }
            public string login { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public int map_count { get; set; }
            public Urls urls { get; set; }
        }

        public class MapDrop {
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public int id { get; set; }
            public string name { get; set; }
            public string base_name { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public int map_tier { get; set; }
            public string image_url { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public int quality { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public int stack_size { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public int item_level { get; set; }
            public string rarity { get; set; }
            public bool unidentified { get; set; }
            public string item_type { get; set; }
            public bool stackable { get; set; }
        }

        public class Map {
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public int id { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public int user_id { get; set; }
            public string display_name { get; set; }
            public string full_name { get; set; }
            public string rarity { get; set; }
            public string image_url { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public int item_rarity { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public int monster_pack_size { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public int map_tier { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public int quality { get; set; }
            public List<string> mods { get; set; }
            public List<string> drops { get; set; }
            public List<string> notes { get; set; }
            public string state { get; set; }
            public string character_name { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public int character_level { get; set; }
            public string character_league { get; set; }
            public string character_class { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public int run_time { get; set; }
            public bool from_clipboard { get; set; }
            public bool unidentified { get; set; }
            public bool corrupted { get; set; }
            public string master_name { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public int fragments { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public int rip_count { get; set; }
            public bool zana { get; set; }
            public User user { get; set; }
            public List<MapDrop> items { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public int item_quantity { get; set; }
            public string crafted_mod { get; set; }
            public bool hardcore { get; set; }
            public string path { get; set; }
        }
    }
}
