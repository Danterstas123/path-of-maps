﻿using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using WindowsInput;

namespace PathOfMapsClient.HelperClasses {
    class WindowsKeySender {
        private static InputSimulator simulator = new InputSimulator();

        public static void PressCtrlC() {
            simulator.Keyboard.KeyDown(WindowsInput.Native.VirtualKeyCode.CONTROL);
            simulator.Keyboard.KeyDown(WindowsInput.Native.VirtualKeyCode.VK_C);
            Thread.Sleep(5);
            simulator.Keyboard.KeyUp(WindowsInput.Native.VirtualKeyCode.CONTROL);
            simulator.Keyboard.KeyUp(WindowsInput.Native.VirtualKeyCode.VK_C);
        }
    }
}
